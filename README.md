# README #

This repository contains AVR project's scaffold for [CodeBlocks IDE](http://www.codeblocks.org).

### Supported AVR devices ###

* ATmega88pa
* ATmega32
* ATmega32u4
* ATmega328

### How do I get set up? ###

* Clone this repository
* Change directory name to your project's name
* Change avr-template.cbp to your project name
* Change project's name in project's properties
* Start coding...

### What if... ###

* If you have any problems or issues - you are welcome to contact me: ton@specadmin.ru