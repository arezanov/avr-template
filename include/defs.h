//-----------------------------------------------------------------------------
// defs.h
//-----------------------------------------------------------------------------
#ifndef DEFS_H
#define DEFS_H
//-----------------------------------------------------------------------------
// AVR includes
#include <avr/io.h>
#include <util/delay.h>
#include <string.h>
// custom includes
#include "config.h"
#include "version.h"
//-----------------------------------------------------------------------------
typedef unsigned char BYTE;
typedef unsigned int WORD;
typedef unsigned long DWORD;
//-----------------------------------------------------------------------------
union WORD_UNION
{
    WORD data;
    BYTE byte[2];
};
//-----------------------------------------------------------------------------
union DWORD_UNION
{
    DWORD data;
    WORD word[2];
    BYTE byte[4];
};
//-----------------------------------------------------------------------------
#define KB  1024
#define MB  1048576
//-----------------------------------------------------------------------------
#define __inline inline __attribute__ ((always_inline))
//-----------------------------------------------------------------------------
#define nop() __asm__ volatile ("nop")
#define disable_interrupts() __asm__ volatile ("cli")
#define enable_interrupts() __asm__ volatile ("sei")
#define bit(b) (BYTE)(1 << (b))
#define set_bit(a,b) (a|=bit(b))
#define clr_bit(a,b) (a&=~bit(b))
#define test_bit(a,b) ((a & bit(b))>>b)
#define delay_cycles(a) __builtin_avr_delay_cycles(a)
#define delay(a) _delay_ms(1000*a)
#define mdelay(a) _delay_ms(a)
#define udelay(a) _delay_us(a)
#define max(a,b) (a>b) ? a : b
#define min(a,b) (a<b) ? a : b
#define halt() while(1);
//-----------------------------------------------------------------------------
#endif

